package com.fransoft.codechallenge.at;

import static org.junit.jupiter.api.Assertions.assertEquals;
import com.intuit.karate.Results;
import com.intuit.karate.Runner;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import net.masterthought.cucumber.Configuration;
import net.masterthought.cucumber.ReportBuilder;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.DisabledIfEnvironmentVariable;

class KarateRunnerTest {

  public static final String PROJECT_NAME = "codechallenge";
  private static String outPutReportPah;
  private Integer numParallelThreadsGet;

  public KarateRunnerTest() {
    outPutReportPah = System.getenv("CODECHALLENGE_KARATE_OUTPUT_PATH");
    try{
      numParallelThreadsGet = Integer.valueOf(System.getenv("CODECHALLENGE_TEST_GET_PARALLEL_NUM_ITEMS"));
    }catch(NumberFormatException e){
      numParallelThreadsGet = 3;
    }
  }

  @Test
  @DisplayName("test con todos los gets")
  @DisabledIfEnvironmentVariable(named = "CODECHALLENGE_TEST_GET", matches = "false")
  void testGetLauncher(){
    Results results = Runner
        .path("classpath:features/get")
        .reportDir(outPutReportPah)
        .parallel(numParallelThreadsGet);
    generateReport();

    assertEquals(0, results.getFailCount(), results.getErrorMessages());
  }

  @Test
  @DisplayName("test de insert")
  @DisabledIfEnvironmentVariable(named = "CODECHALLENGE_TEST_INSERT", matches = "false")
  void testInsertLauncher(){
    Results results = Runner
        .path("classpath:features/insert")
        .reportDir(outPutReportPah)
        .parallel(1);
    generateReport();

    assertEquals(0, results.getFailCount(), results.getErrorMessages());
  }

  public static void generateReport() {
    Collection<File> jsonFiles = FileUtils
        .listFiles(new File(outPutReportPah), new String[] {"json"}, true);
    List<String> jsonPaths = new ArrayList<>(jsonFiles.size());
    jsonFiles.forEach(file -> jsonPaths.add(file.getAbsolutePath()));
    Configuration config = new Configuration(new File(outPutReportPah), PROJECT_NAME);
    new ReportBuilder(jsonPaths, config);
  }
}
