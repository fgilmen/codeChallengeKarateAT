Feature: Insert product

  Background:
      * url baseUrl
      * def ccProductBase = '/product'
      * def statics_path = staticsPath
      * def csvDataValidation = read(staticsPath + '/csv/inserts.csv')

  Scenario Outline: insert products
    Given path ccProductBase
    And request { "id": "<id>", "name": <name>}
    When method POST
    Then status 200
    And match $.callData contains '#notnull'
    And match $.callData.message == 'ok'

    Examples:
      | csvDataValidation |
