Feature: Get price

  Background:
      * url baseUrl
      * def ccPriceBase = '/price'
      * def statics_path = staticsPath
      * def csvDataValidation = read(staticsPath + '/csv/get.csv')

  Scenario Outline: get recommendation by id
    Given path ccPriceBase
    And param applicationDate = "<applicationDate>"
    And param productId = <productId>
    And param brandId = <brandId>
    When method GET
    Then status 200
    And match $.callData contains '#notnull'
    And match $.callData.message == 'ok'
    And match $.productId == '<productId>'
    And match $.brandId == <brandId>
    And match $.finalPrice == '<price>'
    Examples:
      | csvDataValidation |

