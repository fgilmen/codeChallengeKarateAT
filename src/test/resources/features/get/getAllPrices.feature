Feature: Get price

  Background:
      * url baseUrl
      * def ccPriceBase = '/price/all'
      * def statics_path = staticsPath
      * def csvDataValidation = read(staticsPath + '/csv/get_all.csv')

  Scenario Outline: get recommendation by id
    Given path ccPriceBase
    And param productId = <productId>
    When method GET
    Then status 200
    And match $[*].callData contains '#notnull'
    And match $[*].callData.message == ['ok','ok','ok','ok']
    And match $[*].productId == ['<productId>','<productId>','<productId>','<productId>']
    And match $[0].startDate == '<date1>'
    Examples:
      | csvDataValidation |

