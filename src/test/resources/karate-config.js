function fn() {
    var env = karate.env; // get java system property 'karate.env'
    if (!env) {
        env = 'dev'; // a custom 'intelligent' default
    }
    karate.log('karate.env system property was:', env);

    karate.configure('connectTimeout', 5000);
    karate.configure('readTimeout', 5000);

    var protocol = 'http';
    if (java.lang.System.getenv('CODECHALLENGE_API_HTTPS') == 'true') {
        protocol = 'https';
        karate.configure('ssl', true);
    }

    var host = java.lang.System.getenv('CODECHALLENGE_API_HOST');

    var port = java.lang.System.getenv('CODECHALLENGE_API_PORT');
    if (!port) {
        port = ''
    } else {
        port = ':' + port
    }

    var basePath = java.lang.System.getenv('CODECHALLENGE_API_BASEPATH');
    if (!basePath) {
        basePath = ''
    }

    var host_header = java.lang.System.getenv('CODECHALLENGE_API_HOST_HEADER');
    if (host_header) {
        karate.configure('headers', { Host: host_header});
    }

    var api_key = java.lang.System.getenv('CODECHALLENGE_API_KEY');
    if (api_key) {
        karate.configure('headers', { Authorization: 'apiKey ' + api_key});
    }

    var statics_path = java.lang.System.getenv('CODECHALLENGE_KARATE_STATICS_PATH');
    if (!statics_path) {
        statics_path = '/app/statics';
    }

    var config = {
        appId: 'CODECHALLENGE',
        baseUrl : protocol + '://' + host + port + basePath,
        staticsPath : statics_path
    };

    return config;
}
